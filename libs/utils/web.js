(function (self)
{
  "use strict";
  var Fs = require('fs')
  var MimeTypes = require('mime-types');
  
  if (exports === undefined) 
    exports = {};
  
  var WebUtils = 
  {
    LoadResource: function (path, response, status) 
    {
      if (status === undefined) 
        status = 200;
      
      response.writeHead(status, {'Content-Type': MimeTypes.lookup(path, true)});
      response.end(Fs.readFileSync(path));
    }
  };
  
  if (exports !== undefined) 
  {
    exports.LoadResource = WebUtils.LoadResource;
  }
  
  return self;
  
}(this));
