var Hal = require('hal'),
    Express = require('express');

var App = Express();
App.listen(8081, function () 
{
  //Set Handlebars.JS as view engine
  App.set('view engine', 'html');
  App.engine('html', require('hbs').__express);
  
  //Set routing for resources
  App.get(new RegExp('/resource/([^\\/]+?)/(.+)'), function (req, res)
  {
    res.sendFile(__dirname + '/views/' + req.params[1] + '.' + req.params[0]);
  });
   
  App.get('/hello', function (req, res)
  {
    res.render('hello/index');
  });
  
  App.get('/handlebars', function (req, res)
  {
    var data =
    {
      title: 'Practical Node.js',
      author: 
      {
        name: 'Mario Salazar de Torres', 
        web: 'https://bitbucket.org/GaussianR/'
      },
      tags: ['express', 'node', 'javascript']
    };
    
    res.render('handlebars/index', data);
  });
  
  App.get('/api', function (req, res)
  {
    var resource = new Hal.Resource
    ({
      version: 1,
      domain: req.hostname
    }, req.protocol + '://' + req.hostname + ':8081/api');
    
    resource.link('tags', req.protocol + '://' + req.hostname + ':8081/api/tags');
    res.json(resource.toJSON());
  });
  
  App.get('/api/tags', function (req, res)
  {
    var resource = new Hal.Resource
    ({
      tags: ['express', 'node', 'javascript']
    }, req.protocol + '://' + req.hostname + ':8081/api/tags');
    
    resource.link('api', req.protocol + '://' + req.hostname + ':8081/api');
    res.json(resource.toJSON());
  });
  
  //Set 404 page
  App.use(function(req, res)
  {
    res.status(404).render('errors/404/index');
  });
  
  console.log('Express.JS web server started in http://localhost:8081/');
});